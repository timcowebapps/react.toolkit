'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Guid,
	IJsonScheme,
	AlignTypes, TextAlign,
	HtmlTagTypes, ComponentTypes
} from '@timcowebapps/react.utils';

export namespace HeadingProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof HeadingProps.IProps
		 */
		scheme: IJsonScheme;

		/**
		 * Дочерние элементы.
		 *
		 * @type {React.ReactNode}
		 * @memberof HeadingProps.IProps
		 */
		children: any;
	}

	export const types = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				htmlTag: PropTypes.number,
				align: PropTypes.string,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string)
				})
			})
		})
	};

	export const defaults = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				htmlTag: HtmlTagTypes.H3,
				align: TextAlign.toStr(AlignTypes.Center),
				classes: {
					stylesheet: null,
					block: "heading",
					modifiers: []					
				}
			}
		}
	};
}
