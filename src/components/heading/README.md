##Компонент заголовка.

| Свойство | Тип | Значение по умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.htmlTag` | `number` | `HtmlTagTypes.H3` | HTML тег элемента |
| `scheme.properties.align` | `string` | `center` | Выравнивание текста |
| `scheme.properties.classes.stylesheet` | `IStylesheet` | `null` |  |
| `scheme.properties.classes.block` | `string` | `heading` |  |
| `scheme.properties.classes.modifiers` | `Array<string>` | `-` |  |
