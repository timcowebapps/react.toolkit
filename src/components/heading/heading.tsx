'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import {
	Classes, BEM,
	EnumTransform,
	HtmlTagTypes
} from '@timcowebapps/react.utils';
import { HeadingProps } from './heading-props';

const Heading: React.StatelessComponent<HeadingProps.IProps> = (props: HeadingProps.IProps) => {
	const { properties } = _.merge({}, this.default.defaultProps.scheme, props.scheme);
	const { stylesheet, block, modifiers } = properties.classes;

	return React.createElement(EnumTransform.toStr(HtmlTagTypes, properties.htmlTag).toLowerCase(), {
		className: Classes.many(
			BEM.block(stylesheet, block).element().modifiers(modifiers),
			stylesheet[properties.align])
	}, props.children);
};

Heading.propTypes = HeadingProps.types;
Heading.defaultProps = HeadingProps.defaults;

export default Heading;
