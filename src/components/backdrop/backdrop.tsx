'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	Classes, BEM,
	IJsonScheme,
	JsonSchemeHelpers
} from '@timcowebapps/react.utils';
import { BackdropProps } from './backdrop-props';
import { BackdropState } from './backdrop-state';

export default class Backdrop extends React.Component<BackdropProps.IProps, BackdropState.IState> {
	//#region Статические переменные

	public static displayName: string = "Backdrop";
	public static propTypes: PropTypes.ValidationMap<BackdropProps.IProps> = BackdropProps.types;
	public static defaultProps: BackdropProps.IProps = BackdropProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _backdropRef: HTMLElement;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Backdrop
	 * @private
	 * @method _getInitialState
	 */
	private _getInitialState(): BackdropState.IState {
		return {
			// Empty
		};
	}

	/**
	 * Коллбек обработки щелчка мышки.
	 * 
	 * @class Backdrop
	 * @public
	 * @param event 
	 */
	private _handleClick(event: React.SyntheticEvent<HTMLInputElement>): void {
		if (this.props.scheme.properties.onClick)
			this.props.scheme.properties.onClick();
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Backdrop
	 * @public
	 * @constructor
	 * @param {BackdropProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: BackdropProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleClick = this._handleClick.bind(this);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class Backdrop
	 * @public
	 */
	public componentDidMount(): void {
		this._backdropRef.setAttribute("data-effect", this.props.scheme.properties.effect || "none");
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Backdrop
	 * @public
	 * @method render
	 */
	public render(): JSX.Element {
		const { properties, items } = this.props.scheme;
		const { stylesheet, block, element, modifiers } = properties.classes;

		return React.createElement("div", {
			ref: node => { this._backdropRef = node },
			className: BEM.block(stylesheet, block).element(element).modifiers(_.union([
				properties.active ? "is-active" : null
			], modifiers)),
			onClick: this._handleClick
		}, properties.content);
	}
};
