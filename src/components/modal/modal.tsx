'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Classes, BEM,
	IJsonScheme, IJsonSchemeMap, JsonSchemeHelpers,
	ComponentTypes
} from '@timcowebapps/react.utils';
import { ModalProps } from './modal-props';
import { ModalState } from './modal-state';

export default class Modal extends React.Component<ModalProps.IProps, ModalState.IState> {
	//#region Статические переменные

	public static displayName: string = "Modal";
	public static propTypes: PropTypes.ValidationMap<ModalProps.IProps> = ModalProps.types;
	public static defaultProps: ModalProps.IProps = ModalProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _shouldClose: boolean = null; /* Следует закрыть? */

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Modal
	 * @private
	 * @method _getInitialState
	 */
	private _getInitialState(): ModalState.IState {
		return {
			isOpen: this.props.scheme.properties.isOpen
		};
	}

	private _open(): void {
		this.setState({ isOpen: true });
	}

	private _close(): void {
		this.setState({ isOpen: false });
	}

	private _handleOverlayOnClick(event): void {
		if (this._shouldClose === null) {
			this._shouldClose = true;
		}

		if (this._shouldClose) {
			this._requestClose(event);
		}

		this._shouldClose = null;
	}

	private _handleContentOnClick(event): void {
		this._shouldClose = false;
	}

	private _requestClose(event): void {
		this.setState({ isOpen: false });

		if (this.props.scheme.properties.onRequestClose)
			this.props.scheme.properties.onRequestClose(event);
	}

	private _shouldBeClosed(): boolean {
		return !this.state.isOpen;
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Modal
	 * @public
	 * @constructor
	 * @param {ModalProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ModalProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleOverlayOnClick = this._handleOverlayOnClick.bind(this);
		this._handleContentOnClick = this._handleContentOnClick.bind(this);
	}

	/**
	 * Компонент получает новые props.
	 * Этод метод не вызывается в момент первого render'a.
	 * 
	 * @class Modal
	 * @public
	 * @param {ModalProps.IProps} nextProps Новые свойства.
	 */
	public componentWillReceiveProps(nextProps: ModalProps.IProps): void {
		if (!this.props.scheme.properties.isOpen && nextProps.scheme.properties.isOpen) {
			this._open();
		} else if (this.props.scheme.properties.isOpen && !nextProps.scheme.properties.isOpen) {
			this._close();
		}
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Modal
	 * @public
	 * @method render
	 */
	public render(): JSX.Element {
		const { properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers } = properties.classes;

		return this._shouldBeClosed() ? null : (
			<div className={stylesheet["overlay"]} onClick={this._handleOverlayOnClick}>
				<div className={BEM.block(stylesheet, block).element("content").toStr()} onClick={this._handleContentOnClick}>
					{this.props.children}
				</div>
			</div>
		);
	}
};
