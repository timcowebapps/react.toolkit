'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme } from '@timcowebapps/react.utils';

export namespace ModalProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Дочерние элементы.
		 *
		 * @type {React.ReactNode}
		 * @memberof ModalProps.IProps
		 */
		children?: React.ReactNode;

		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof ModalProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			isOpen: PropTypes.bool,
			onRequestClose: PropTypes.func
	})
	};

	export const defaults: IProps = {
		scheme: {
			properties: {
				isOpen: false,
				onRequestClose: undefined,
				classes: {
					stylesheet: undefined,
					block: "modal"
				}
			}
		}
	};
}
