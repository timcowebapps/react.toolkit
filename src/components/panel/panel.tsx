'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Classes, BEM,
	IJsonScheme,
	JsonSchemeHelpers
} from '@timcowebapps/react.utils';
import { PanelProps } from './panel-props';
import { PanelState } from './panel-state';

export default class Panel extends React.Component<PanelProps.IProps, PanelState.IState> {
	//#region Статические переменные

	public static displayName: string = "Panel";
	public static propTypes: PropTypes.ValidationMap<PanelProps.IProps> = PanelProps.types;
	public static defaultProps: PanelProps.IProps = PanelProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Panel
	 * @private
	 * @method _getInitialState
	 */
	private _getInitialState(): PanelState.IState {
		return {
			// Empty
		};
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Panel
	 * @public
	 * @constructor
	 * @param {PanelProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: PanelProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Panel
	 * @public
	 * @method render
	 */
	public render(): JSX.Element {
		const { properties, items } = this.props.scheme;
		const { stylesheet, block, element, modifiers } = properties.classes;

		return React.createElement("div", {
			role: properties.role || undefined,
			className: BEM.block(stylesheet, block).element(element).modifiers(modifiers)
		}, properties.content);
	}
};
