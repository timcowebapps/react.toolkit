'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme, ComponentTypes } from '@timcowebapps/react.utils';

export namespace PanelProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof PanelProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			type: PropTypes.number,
			properties: PropTypes.shape({
				//Empty
			})
		})
	};

	export const defaults: IProps = {
		scheme: {
			type: ComponentTypes.Panel,
			properties: {
				// Empty
			}
		}
	};
}
