'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Guid,
	IJsonScheme,
	HtmlTagTypes, ComponentTypes
} from '@timcowebapps/react.utils';

export namespace LabelProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof LabelProps.IProps
		 */
		scheme?: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			type: PropTypes.number,
			properties: PropTypes.shape({
				htmlTag: PropTypes.number,
				associateWith: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
				text: PropTypes.string.isRequired,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string)
				})
			})
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			type: ComponentTypes.Label,
			properties: {
				htmlTag: HtmlTagTypes.Span,
				associateWith: "",
				text: "",
				classes: {
					stylesheet: null,
					block: "label",
					modifiers: []
				}
			}
		}
	}
}
