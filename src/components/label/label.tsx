'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	BEM,
	EnumTransform,
	HtmlTagTypes
 } from '@timcowebapps/react.utils';
import { LabelProps } from './label-props';

export default class Label extends React.Component<LabelProps.IProps, any> {
	//#region Статические переменные

	public static displayName: string = "Label";
	public static propTypes: PropTypes.ValidationMap<LabelProps.IProps> = LabelProps.types;
	public static defaultProps = LabelProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Label
	 * @public
	 * @constructor
	 * @param {LabelProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: LabelProps.IProps) {
		super(props);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Label
	 * @public
	 * @return {Component}
	 */
	public render(): JSX.Element {
		const { properties, items } = _.merge({}, Label.defaultProps.scheme, this.props.scheme);
		const { stylesheet, block, element, modifiers } = properties.classes;

		return React.createElement(EnumTransform.toStr(HtmlTagTypes, properties.htmlTag).toLowerCase(), {
			htmlFor: properties.associateWith,
			className: BEM.block(stylesheet, block).element(element).modifiers(modifiers)
		}, properties.text);
	}
}
