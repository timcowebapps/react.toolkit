##Компонент иконки.

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.htmlTag` | `number` | `span` | Имя тега |
| `scheme.properties.associateWith` | `string` | `-` | |
| `scheme.properties.text` | `string` | `-` | Текст |
