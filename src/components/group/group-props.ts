'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme } from '@timcowebapps/react.utils';

export namespace GroupProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof GroupProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			// Empty
		})
	}

	export const defaults: IProps = {
		scheme: {
			// Empty
		}
	}
}
