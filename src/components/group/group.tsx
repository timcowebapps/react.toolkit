'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, JsonSchemeHelpers, IJsonSchemeMap
} from '@timcowebapps/react.utils';
import { GroupProps } from './group-props';
import { GroupState } from './group-state';
import Button from './../button/button';

/**
 * Микрокомпонент переключателя.
 */
export default class Group extends React.Component<GroupProps.IProps, GroupState.IState> {
	//#region Статические переменные

	public static displayName: string = "Group";
	public static propTypes: PropTypes.ValidationMap<GroupProps.IProps> = GroupProps.types;
	public static defaultProps: GroupProps.IProps = GroupProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Group
	 * @private
	 */
	private _getInitialState(): GroupState.IState {
		return {
			// Empty
		};
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Group
	 * @public
	 * @constructor
	 * @param {GroupProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: GroupProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Group
	 * @public
	 */
	public render() {
		const { properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers } = properties.classes;

		return React.createElement("div", {
			// Empty
		}, (items as IJsonSchemeMap).map((item: IJsonScheme, idx: number) => {
			return <Button key={idx} scheme={item} />
		}));
	}
}
