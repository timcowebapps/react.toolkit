'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, JsonSchemeHelpers, IJsonSchemeMap,
	EnumTransform,
	HtmlTagTypes
} from '@timcowebapps/react.utils';
import { NavbarProps } from './navbar-props';
import { NavbarState } from './navbar-state';
import Button from './../button/button';

/**
 * Микрокомпонент переключателя.
 */
export default class Navbar extends React.Component<NavbarProps.IProps, NavbarState.IState> {
	//#region Статические переменные

	public static displayName: string = "Navbar";
	public static propTypes: PropTypes.ValidationMap<NavbarProps.IProps> = NavbarProps.types;
	public static defaultProps: NavbarProps.IProps = NavbarProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Navbar
	 * @private
	 */
	private _getInitialState(): NavbarState.IState {
		return {
			// Empty
		};
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Navbar
	 * @public
	 * @constructor
	 * @param {NavbarProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: NavbarProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Navbar
	 * @public
	 */
	public render() {
		const { properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers } = properties.classes;

		return React.createElement(EnumTransform.toStr(HtmlTagTypes, properties.htmlTag).toLowerCase(), {
			itemScope: true,
			itemType: _.isEqual(properties.htmlTag, HtmlTagTypes.Aside) ? "https://schema.org/WPSideBar" : "https://schema.org/WPHeader",
			role: "banner",
			className: BEM.block(stylesheet, block).element().modifiers(modifiers)
		}, this.props.children);
	}
}
