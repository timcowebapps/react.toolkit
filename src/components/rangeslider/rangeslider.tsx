'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	Guid,
	Classes, BEM,
	EnumTransform,
	OrientTypes, OrientTransform
} from '@timcowebapps/react.utils';
import { RangeSliderProps } from './rangeslider-props';
import { RangeSliderState } from './rangeslider-state';
import { ITrackBarHtmlElement, TrackBar } from './trackbar';
import { ITrackBarRangeHtmlElement, TrackBarRange } from './trackbar-range';
import { IHandleHtmlElement, Handle } from './handle';

export default class RangeSlider extends React.Component<RangeSliderProps.IProps, RangeSliderState.IState> {
	//#region Статические переменные

	public static displayName: string = "RangeSlider";
	public static propTypes: PropTypes.ValidationMap<RangeSliderProps.IProps> = RangeSliderProps.types;
	public static defaultProps: RangeSliderProps.IProps = RangeSliderProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _isRanged: boolean;

	/** HTML элемент трека. */
	private _trackBarRef: ITrackBarHtmlElement;
	private _trackBarPosition: { x: number, y: number };

	/** HTML элемент диапазона. */
	private _trackBarRangeRef: ITrackBarRangeHtmlElement;

	private _trackFactor: number;

	/** HTML элемент ползунка. */
	private _handleRef: IHandleHtmlElement;
	private _handleSize;
	private _handleGrabPosition: number;

	//#endregion

	//#region Приватные методы

	/** 
	 * 
	 * @class RangeSlider
	 * @public
	 */
	private _recalculateTrack(): void {
		const { orientation, min, max } = this.props.scheme.properties;
		const axis: string = OrientTransform.toAxis(orientation);

		if (!_.isUndefined(this._trackBarRef)) {
			this.setState({
				trackBarLength: this._trackBarRef.getTrackBarDimensions()[axis]
			});

			this._trackBarPosition = this._trackBarRef.getTrackBarPosition();
			this._trackFactor = this.state.trackBarLength / (max - min);
		}
	}

	/** 
	 * 
	 * @class RangeSlider
	 * @public
	 */
	private _recalculateHandle(): void {
		if (!_.isUndefined(this._handleRef)) {
			this._handleSize = this._handleRef.getHandleSize();
			this._handleGrabPosition = this._handleSize / 2;
		}
	}

	/**
	 * 
	 * @class RangeSlider
	 * @public
	 * @param {number} position 
	 */
	private _getStepValue(position: number): number {
		const { step } = this.props.scheme.properties;
		const remainder = position % step;

		if (remainder < step / 2)
			return position - remainder;

		return position - remainder + step;
	}

	/**
	 * 
	 * @class RangeSlider
	 * @public
	 * @param {number} start 
	 */
	private _getStartValue(start: number): number {
		let value = this._getStepValue(start);

		if (value < this.props.scheme.properties.min) {
			value = this.props.scheme.properties.min;
		} else if (value > this.props.scheme.properties.max) {
			value = this.props.scheme.properties.max;
		}

		return value;
	}

	private _getPositionFromStartValue() {
		const { min, max } = this.props.scheme.properties;
		let value = 0, percentage = 0;

		if (this.state.start < min || this.state.end < min) {
			value = min;
		} else if (this.state.start > Math.min(this.state.end, max)) {
			value = Math.min(this.state.end, max);
		} else {
			value = this.state.start;
		}

		// В процентах
		// percentage = ((value - min) / (max - min));
		// return Number.isNaN(percentage) ? 0 : percentage * 100;

		// В пикселях
		percentage = (value - min) / (max - min);
		return (isNaN(percentage)) ? 0 : percentage * (this.state.trackBarLength - this._handleSize);
	}

	/**
	 * 
	 */
	private _handleStart = (event: any): void => {
		document.addEventListener('mousemove', this._handleDrag);
		document.addEventListener('mouseup', this._handleEnd);
	}

	/**
	 * Получает координаты позиции курсора.
	 * 
	 * @param event 
	 */
	private _getPointerCoord(event: any): { x: number, y: number } {
		if (event) {
			const changedTouches = (event as TouchEvent).changedTouches;
			if (changedTouches && changedTouches.length > 0) {
				const touch = changedTouches[0];
				return { x: touch.clientX, y: touch.clientY };
			}

			if ((event as MouseEvent).pageX !== undefined) {
				return { x: (event as MouseEvent).pageX, y: (event as MouseEvent).pageY };
			}
		}

		return { x: 0, y: 0 };
	}

	/**
	 * 
	 */
	private _handleDrag = (event: any): void => {
		event.preventDefault();

		const { orientation, min, max } = this.props.scheme.properties;
		const axis: string = OrientTransform.toAxis(orientation);

		let mouseDownPosition = this._getPointerCoord(event)[axis] - this._trackBarPosition[axis] + (min * this._trackFactor);
		let newStart = this._getStartValue((mouseDownPosition - this._handleGrabPosition) / this._trackFactor);

		this.props.scheme.properties.onChange(this._isRanged ? {
			start: newStart,
			end: max
		} : newStart);
	}

	/**
	 * 
	 */
	private _handleEnd = (event: any): void => {
		document.removeEventListener('mousemove', this._handleDrag);
		document.removeEventListener('mouseup', this._handleEnd);
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class RangeSlider
	 * @public
	 * @constructor
	 * @param {RangeSliderProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: RangeSliderProps.IProps) {
		super(props);

		this._isRanged = _.isObject(this.props.scheme.properties.value);
		this.state = {
			trackBarLength: 0,
			start: this._isRanged ? this.props.scheme.properties.value.start : this.props.scheme.properties.value,
			end: this._isRanged ? this.props.scheme.properties.value.end : this.props.scheme.properties.max
		};

		this._handleStart = this._handleStart.bind(this);
		this._handleDrag = this._handleDrag.bind(this);
		this._handleEnd = this._handleEnd.bind(this);
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class RangeSlider
	 * @public
	 */
	public componentWillMount(): void {
		this.props = _.merge({}, RangeSlider.defaultProps, this.props);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class RangeSlider
	 * @public
	 */
	public componentDidMount(): void {
		this._recalculateTrack();
		this._recalculateHandle();
	}

	/**
	 * Вызывается сразу после render.
	 * Не вызывается в момент первого render'а компонента.
	 * 
	 * @class RangeSlider
	 * @public
	 * @param {RangeSliderProps.IProps} prevProps Предыдушее значение свойств.
	 * @param {RangeSliderState.IState} prevState Предыдущее значение состояния.
	 */
	public componentDidUpdate(prevProps: RangeSliderProps.IProps, prevState: RangeSliderState.IState): void {
		if (this._trackBarRef.getTrackBarDimensions()[OrientTransform.toAxis(this.props.scheme.properties.orientation)] !== prevState.trackBarLength) {
			this._recalculateTrack();
			this._recalculateHandle();
		}
	}

	/**
	 * Компонент получает новые props.
	 * Этод метод не вызывается в момент первого render'a.
	 * 
	 * @class RangeSlider
	 * @public
	 * @param {RangeSliderProps.IProps} nextProps Новые свойства.
	 */
	public componentWillReceiveProps(nextProps: RangeSliderProps.IProps): void {
		const { value, max } = nextProps.scheme.properties;

		this._isRanged = _.isObject(value);
		this.setState({
			start: this._isRanged ? value.start : value,
			end: this._isRanged ? value.end : max
		});
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class RangeSlider
	 * @public
	 */
	public render() {
		this.props = _.merge({}, RangeSlider.defaultProps, this.props);

		const { uniqueId, properties } = this.props.scheme;
		const { stylesheet, block } = properties.classes;

		return (
			<div className={stylesheet[block]} onMouseDown={this._handleDrag} onMouseUp={this._handleEnd}>
				<TrackBar ref={node => { this._trackBarRef = node }} scheme={{
					uniqueId: uniqueId + "-track-bar",
					properties: {
						orientation: properties.orientation,
						classes: {
							stylesheet: stylesheet,
							block: block
						}
					}
				}}>
					<TrackBarRange ref={node => { this._trackBarRangeRef = node }} scheme={{
						uniqueId: uniqueId + "-track-bar-range",
						properties: {
							orientation: properties.orientation,
							offset: 0,
							length: this._getPositionFromStartValue(),
							classes: {
								stylesheet: stylesheet,
								block: block
							}
						}
					}} />
				</TrackBar>

				<Handle ref={node => { this._handleRef = node }} scheme={{
					uniqueId: uniqueId + "-handle",
					properties: {
						offset: this._getPositionFromStartValue(),
						orientation: properties.orientation,
						onMouseDown: this._handleStart,
						classes: {
							stylesheet: stylesheet,
							block: block
						}
					}
				}} />
			</div>
		);
	}
}
