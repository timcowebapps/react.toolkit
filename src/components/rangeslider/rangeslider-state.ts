'use strict';

export namespace RangeSliderState {
	export interface IState {
		/**
		 * Длина трека.
		 * 
		 * @type {number}
		 * @memberof RangeSliderState.IState
		 */
		trackBarLength: number;

		start: number;
		end: number;
	}
}
