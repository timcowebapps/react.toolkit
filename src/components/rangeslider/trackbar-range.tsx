'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	EnumTransform,
	OrientTypes, OrientTransform, OrientHelpers
} from '@timcowebapps/react.utils';
import { TrackBarRangeProps } from './trackbar-range-props';

export interface ITrackBarRangeHtmlElement extends HTMLElement {
	getTrackBarRangePosition: () => { x: number, y: number };
	getTrackBarRangeDimensions: () => { x: number, y: number };
}

export class TrackBarRange extends React.Component<TrackBarRangeProps.IProps, any> {
	//#region Статические переменные

	public static displayName: string = "TrackBarRange";
	public static propTypes: PropTypes.ValidationMap<TrackBarRangeProps.IProps> = TrackBarRangeProps.types;
	public static defaultProps: TrackBarRangeProps.IProps = TrackBarRangeProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _trackBarRangeRef: SVGElement;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class TrackBarRange
	 * @private
	 */
	private _getInitialState(): any {
		return {
			// Empty
		}
	}

	private _renderDefs(): JSX.Element {
		const { uniqueId, properties } = this.props.scheme;
		const { orientation, offset, length, classes } = properties;

		return <clipPath id={uniqueId.split("-")[1]}>
			<rect
				x={(OrientHelpers.isEqual(OrientTypes.Horizontal, orientation) ? offset : 0) + "px"}
				y={(OrientHelpers.isEqual(OrientTypes.Vertical, orientation) ? offset : 0) + "px"}
				width={(OrientHelpers.isEqual(OrientTypes.Horizontal, orientation) ? (_.isNaN(length) ? 0 : length) : classes.stylesheet.rangebar_active_thickness)}
				height={(OrientHelpers.isEqual(OrientTypes.Vertical, orientation) ? (_.isNaN(length) ? 0 : length) : classes.stylesheet.rangebar_active_thickness)} />
		</clipPath>
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class TrackBarRange
	 * @public
	 * @constructor
	 * @param {TrackBarRangeProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: TrackBarRangeProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Получает позицию дорожки.
	 * 
	 * @class TrackBarRange
	 * @public
	 */
	public getTrackBarRangePosition(): { x: number, y: number } {
		const boundingClientRect = this._trackBarRangeRef.getBoundingClientRect();
		return {
			x: boundingClientRect.left,
			y: boundingClientRect.top
		}
	}

	/** 
	 * Получает габариты дорожки.
	 * 
	 * @class TrackBarRange
	 * @public
	 */
	public getTrackBarRangeDimensions(): { x: number, y: number } {
		return {
			x: this._trackBarRangeRef.clientWidth,
			y: this._trackBarRangeRef.clientHeight
		};
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class TrackBarRange
	 * @public
	 */
	public render() {
		const { uniqueId, properties } = this.props.scheme;
		const { block, modifiers } = properties.classes;
		const stylesheet: IStylesheet = properties.classes.stylesheet;

		return React.createElement("svg", {
			key: uniqueId,
			ref: node => { this._trackBarRangeRef = node },
			className: BEM.block(stylesheet, block).element("trackbar-range").modifiers([
				OrientTransform.toLowerStr(properties.orientation)
			]),
			style: {
				width: OrientHelpers.isEqual(OrientTypes.Horizontal, properties.orientation) ? stylesheet.rangebar_active_length : stylesheet.rangebar_active_thickness,
				height: OrientHelpers.isEqual(OrientTypes.Vertical, properties.orientation) ? stylesheet.rangebar_active_length : stylesheet.rangebar_active_thickness
			}
		},
			<React.Fragment>
				<defs>{this._renderDefs()}</defs>
				<linearGradient id={uniqueId.split("-")[2]} gradientUnits="userSpaceOnUse" x1="0" y1="0" x2="100%" y2="0">
					<stop offset="0" stopColor={stylesheet.rangebar_active_background_start} />
					<stop offset="1" stopColor={stylesheet.rangebar_active_background_end} />
				</linearGradient>
				<rect
					fill={"url(#" + uniqueId.split("-")[2] + ")"}
					width={OrientHelpers.isEqual(OrientTypes.Horizontal, properties.orientation) ? stylesheet.rangebar_active_length : stylesheet.rangebar_active_thickness}
					height={OrientHelpers.isEqual(OrientTypes.Vertical, properties.orientation) ? stylesheet.rangebar_active_length : stylesheet.rangebar_active_thickness}
					clipPath={"url(#" + uniqueId.split("-")[1] + ")"}
					rx={stylesheet.rangebar_active_border_radius}
					ry={stylesheet.rangebar_active_border_radius} />
			</React.Fragment>)
	}
}
