'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace HandleProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof HandleProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				offset: PropTypes.number,
				onMouseDown: PropTypes.func,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string
				})
			})
		}).isRequired
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				offset: 0,
				onMouseDown: undefined,
				classes: {
					stylesheet: null,
					block: "handle"
				}
			}
		}
	}
}
