'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Classes, BEM,
	EnumTransform,
	OrientTypes, OrientTransform
} from '@timcowebapps/react.utils';
import { TrackBarProps } from './trackbar-props';

export interface ITrackBarHtmlElement extends HTMLElement {
	getTrackBarPosition: () => { x: number, y: number };
	getTrackBarDimensions: () => { x: number, y: number };
}

export class TrackBar extends React.Component<TrackBarProps.IProps, any> {
	//#region Статические переменные

	public static displayName: string = "TrackBar";
	public static propTypes: PropTypes.ValidationMap<TrackBarProps.IProps> = TrackBarProps.types;
	public static defaultProps: TrackBarProps.IProps = TrackBarProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _trackBarRef: HTMLElement;

	//#endregion

	//#region Приватные методы

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class TrackBar
	 * @public
	 * @constructor
	 * @param {TrackBarProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: TrackBarProps.IProps) {
		super(props);
	}

	/**
	 * Получает позицию дорожки.
	 * 
	 * @class TrackBar
	 * @public
	 */
	public getTrackBarPosition(): { x: number, y: number } {
		const boundingClientRect = this._trackBarRef.getBoundingClientRect();
		return {
			x: boundingClientRect.left,
			y: boundingClientRect.top
		}
	}

	/** 
	 * Получает габариты дорожки.
	 * 
	 * @class TrackBar
	 * @public
	 */
	public getTrackBarDimensions(): { x: number, y: number } {
		return {
			x: this._trackBarRef.clientWidth,
			y: this._trackBarRef.clientHeight
		};
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class TrackBar
	 * @public
	 */
	public render() {
		const { uniqueId, properties } = this.props.scheme;
		const { stylesheet, block } = properties.classes;

		return React.createElement("div", {
			key: uniqueId,
			ref: node => { this._trackBarRef = node },
			className: BEM.block(stylesheet, block).element("trackbar").modifiers([
				OrientTransform.toLowerStr(properties.orientation)
			])
		}, this.props.children);
	}
}
