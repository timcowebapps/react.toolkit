'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme, OrientTypes } from '@timcowebapps/react.utils';

export namespace TrackBarRangeProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof TrackBarRangeProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				orientation: PropTypes.number,
				offset: PropTypes.number,
				length: PropTypes.number,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string
				})
			})
		}).isRequired
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				orientation: OrientTypes.Horizontal,
				offset: 0,
				length: 0,
				classes: {
					stylesheet: null,
					block: "trackbar-range"
				}
			}
		}
	}
}
