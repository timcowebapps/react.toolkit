'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Classes, BEM,
	EnumTransform,
	OrientTypes, OrientHelpers
} from '@timcowebapps/react.utils';
import { HandleProps } from './handle-props';
import { HandleState } from './handle-state';

export interface IHandleHtmlElement extends HTMLElement {
	getHandleSize: () => number;
}

export class Handle extends React.Component<HandleProps.IProps, HandleState.IState> {
	//#region Статические переменные

	public static displayName: string = "Handle";
	public static propTypes: PropTypes.ValidationMap<HandleProps.IProps> = HandleProps.types;
	public static defaultProps: HandleProps.IProps = HandleProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _handleRef: HTMLElement;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Handle
	 * @private
	 */
	private _getInitialState(): HandleState.IState {
		return {
			hovered: false,
			focused: false,
			active: false
		}
	}

	/**
	 * Обработчик фокуса комнонента.
	 * 
	 * @class Handle
	 * @private
	 */
	private _handleFocus(): void {
		this.setState({ focused: true });
	}

	/**
	 * Обработчик снятия фокуса компонента.
	 * 
	 * @class Handle
	 * @private
	 */
	private _handleBlur(): void {
		this.setState({ focused: false });
	}

	/**
	 * Обработчик события наведения курсора на ползунок.
	 * 
	 * @class Handle
	 * @private
	 */
	private _handleMouseEnter(): void {
		this.setState({ hovered: true });
	}

	/**
	 * Обработчик события снятия курсора с ползунка.
	 * 
	 * @class Handle
	 * @private
	 */
	private _handleMouseLeave(): void {
		this.setState({ hovered: false });
	}

	private _handleMouseDown(event: React.SyntheticEvent<HTMLDivElement>): void {
		this.props.scheme.properties.onMouseDown(event);
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Handle
	 * @public
	 * @constructor
	 * @param {HandleProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: HandleProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleFocus = this._handleFocus.bind(this);
		this._handleBlur = this._handleBlur.bind(this);
		this._handleMouseEnter = this._handleMouseEnter.bind(this);
		this._handleMouseLeave = this._handleMouseLeave.bind(this);
		this._handleMouseDown = this._handleMouseDown.bind(this);
	}

	/**
	 * 
	 */
	public getHandleSize(): number {
		return this._handleRef.clientWidth;
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Handle
	 * @public
	 */
	public render() {
		const { uniqueId, properties } = this.props.scheme;
		const { stylesheet, block } = properties.classes;

		return (
			React.createElement("div", {
				key: uniqueId,
				ref: node => { this._handleRef = node },
				className: BEM.block(stylesheet, block).element("handle").modifiers([
					this.state.focused ? "focused" : "",
					this.state.hovered ? "hovered" : ""
				]),
				style: {
					left: OrientHelpers.isEqual(OrientTypes.Horizontal, properties.orientation) ? `${properties.offset}px` : 0,
					top: OrientHelpers.isEqual(OrientTypes.Vertical, properties.orientation) ? `${properties.offset}px` : 0
				},
				onFocus: this._handleFocus,
				onBlur: this._handleBlur,
				onMouseEnter: this._handleMouseEnter,
				onMouseLeave: this._handleMouseLeave,
				onMouseDown: this._handleMouseDown
			})
		);
	}
}
