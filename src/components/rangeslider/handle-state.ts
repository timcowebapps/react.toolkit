'use strict';

export namespace HandleState {
	export interface IState {
		hovered: boolean;
		focused: boolean;
		active: boolean;
	}
}
