'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Guid,
	IJsonScheme,
	OrientTypes
} from '@timcowebapps/react.utils';

export namespace RangeSliderProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof RangeSliderProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				min: PropTypes.number,
				max: PropTypes.number,
				step: PropTypes.number,
				value: PropTypes.oneOfType([
					PropTypes.number,
					PropTypes.PropTypes.shape({
						start: PropTypes.number,
						end: PropTypes.number
					})
				]),
				onChange: PropTypes.func,
				orientation: PropTypes.number,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string
				})
			})
		}).isRequired
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				min: 0, max: 10, step: 1,
				value: null,
				onChange: undefined,
				orientation: OrientTypes.Horizontal,
				classes: {
					stylesheet: null,
					block: "rangeslider"
				}
			}
		}
	}
}
