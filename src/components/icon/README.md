##Компонент иконки.

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.width` | `string | number` | `1em` | Ширина элемента |
| `scheme.properties.height` | `string | number` | `1em` | Высота элемента |
| `scheme.properties.viewBox.x` | `number` | `0` | Координата по оси X отображаемой области |
| `scheme.properties.viewBox.y` | `number` | `0` | Координата по оси Y отображаемой области |
| `scheme.properties.viewBox.width` | `number` | `32` | Ширина отображаемой области |
| `scheme.properties.viewBox.height` | `number` | `32` | Высота отображаемой области |
| `scheme.properties.preserveAspectRatio` | `string` | `none` | Выравнивание и поведение |
