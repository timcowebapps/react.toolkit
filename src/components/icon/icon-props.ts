'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export enum SvgBehaviorTypes {
	Meet,
	Slice
}

export enum SvgAlignmentTypes {
	xMin, YMin,
	xMid, YMid,
	xMax, YMax
}

export namespace IconProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof IconProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			properties: PropTypes.shape({
				width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
				height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
				viewBox: PropTypes.shape({
					x: PropTypes.number,
					y: PropTypes.number,
					width: PropTypes.number,
					height: PropTypes.number
				}),
				preserveAspectRatio: PropTypes.string
			})
		})
	}

	export const defaults: IProps = {
		scheme: {
			properties: {
				width: "1em", height: "1em",
				viewBox: { x: 0, y: 0, width: 32, height: 32 },
				preserveAspectRatio: "none"
			}
		}
	}
}
