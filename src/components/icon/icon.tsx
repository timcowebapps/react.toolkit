'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import { Guid, BEM, IJsonScheme, JsonSchemeHelpers } from '@timcowebapps/react.utils';
import { IconProps } from './icon-props';
import { IconState } from './icon-state';

export default class Icon extends React.Component<IconProps.IProps, IconState.IState> {
	//#region Статические переменные

	public static displayName: string = "Icon";
	public static propTypes: PropTypes.ValidationMap<IconProps.IProps> = IconProps.types;
	public static defaultProps = IconProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Icon
	 * @private
	 */
	private _getInitialState(): IconState.IState {
		return {
			// Empty
		};
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Icon
	 * @public
	 * @constructor
	 * @param {IconProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: IconProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Icon
	 * @public
	 */
	public render(): JSX.Element {
		const { properties, items } = _.merge({}, Icon.defaultProps.scheme, this.props.scheme);
		const { stylesheet, block, modifiers } = properties.classes;

		return React.createElement("svg", {
			version: 1.1,
			xmlns: "http://www.w3.org/2000/svg",
			xmlnsXlink: "http://www.w3.org/1999/xlink",
			width: properties.width,
			height: properties.height,
			viewBox: [properties.viewBox.x, properties.viewBox.y, properties.viewBox.width, properties.viewBox.height].join(" "),
			preserveAspectRatio: properties.preserveAspectRatio,
			className: BEM.block(stylesheet, block).element().modifiers(modifiers)
		}, React.createElement("g", {
			// Empty
		}, (this.props.scheme.items as Array<IJsonScheme>).map((value: IJsonScheme, key: number) => {
			return <path key={key} d={value.properties.geometry} />
		})))
	}
}
