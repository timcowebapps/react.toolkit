'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';
import { LabelProps } from '../label/label-props';

export namespace CheckboxProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof CheckboxProps.IProps
		 */
		scheme?: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			properties: PropTypes.shape({
			}),
			items: PropTypes.arrayOf(PropTypes.oneOfType([
				PropTypes.shape({
				}),
				PropTypes.shape(LabelProps.types.scheme)
			]))
		})
	};

	export const defaults: IProps = {
		scheme: {
			properties: {
				classes: {
					stylesheet: null,
					block: "checkbox"
				}
			},
			items: [{
				properties: {
					/** Имя компонента в DOM. */
					name: "",
					/** Управление состоянием вкл/выкл компонента. */
					checked: false
				}
			}]
		}
	};
}
