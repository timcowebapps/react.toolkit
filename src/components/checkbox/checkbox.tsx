'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Guid,
	IStylesheet, Classes, BEM,
	JsonSchemeHelpers,
	HtmlTagTypes, ComponentTypes
} from '@timcowebapps/react.utils';
import { CheckboxState } from './checkbox-state';
import { CheckboxProps } from './checkbox-props';
import Label from '../label/label';
import { LabelProps } from '../label/label-props';

export default class Checkbox extends React.Component<CheckboxProps.IProps, CheckboxState.IState> {
	//#region Статические переменные

	public static displayName: string = "Checkbox";
	public static propTypes: PropTypes.ValidationMap<CheckboxProps.IProps> = CheckboxProps.types;
	public static defaultProps: CheckboxProps.IProps = CheckboxProps.defaults;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Checkbox
	 * @private
	 */
	private _getInitialState(): CheckboxState.IState {
		return {
			hovered: false,
			focused: false
		};
	}

	/**
	 * Обработчик фокуса комнонента.
	 * 
	 * @class Checkbox
	 * @private
	 * @param event 
	 */
	private _handleFocus(event: any/*JQueryEventObject*/): void {
		this.setState({ focused: true });
	}

	/**
	 * Обработчик снятия фокуса компонента.
	 * 
	 * @class Checkbox
	 * @private
	 * @param event 
	 */
	private _handleBlur(event: any/*JQueryEventObject*/): void {
		this.setState({ focused: false });
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Checkbox
	 * @public
	 * @constructor
	 * @param {CheckboxProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: CheckboxProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleFocus = this._handleFocus.bind(this);
		this._handleBlur = this._handleBlur.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Checkbox
	 * @public
	 */
	public render() {
		const { uniqueId, properties, items } = this.props.scheme;
		const { block, modifiers } = properties.classes;
		const stylesheet: IStylesheet = properties.classes.stylesheet;

		const inputSchema = JsonSchemeHelpers.getItemById(items, 'input');
		const labelScheme = JsonSchemeHelpers.getItemByType(items, ComponentTypes.Label);

		return (
			React.createElement("div", {
				className: BEM.block(stylesheet, block).element().modifiers([
					inputSchema.properties.checked ? "checked" : "",
					inputSchema.properties.disabled ? "disabled" : "",
					this.state.hovered ? "hovered" : "",
					this.state.focused ? "focused" : ""
				]),
				onFocus: this._handleFocus,
				onBlur: this._handleBlur
			},
				<React.Fragment>
					<div className={BEM.block(stylesheet, block).element("box").modifiers([
						// Empty
					])}>
						<input className={BEM.block(stylesheet, [block, "box"]).element("input").toStr()} type="checkbox" name={inputSchema.properties.name} id={properties.value} value={properties.value} checked={inputSchema.properties.checked} onChange={properties.onChange} />
						<span className={BEM.block(stylesheet, [block, "box"]).element("icon").toStr()} />
					</div>
					<Label scheme={_.merge({}, LabelProps.defaults.scheme, {
						properties: {
							htmlTag: HtmlTagTypes.Label,
							associateWith: properties.value,
							text: labelScheme.properties.text,
							classes: {
								stylesheet: stylesheet
							}
						}
					})} />
				</React.Fragment>
			)
		);
	}
}
