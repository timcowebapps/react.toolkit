##Компонент чекбокса.

###Свойства scheme 

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.classes.stylesheet` | `IStylesheet` | `null` |  |
| `scheme.properties.classes.block` | `string` | `checkbox` |  |

###Переменные sass

| Свойство | По умолчанию | Описание |
| :--- | :--- | :--- |
| `$checkbox-name` | `checkbox` | Имя чекбокса |
| `$checkbox-background` | `#fff` | Фон чекбокса |
| `$checkbox-background--hover` | `#ccc` | Фон чекбокса при наведении курсора |
| `$checkbox-background--checked` | `#000` | Фон выбранного чекбокса |
| `$checkbox-background--checked--hover` | `#ccc` | Фон выбранного чекбокса при наведении курсора |
| `$checkbox-borderColor` | `#000` | Цвет обводка чекбокса |
| `$checkbox-borderRadius` | `none` | Скругление углов чекбокса |
| `$checkbox-width` | `20px` | Размер чекбокса по ширине |
| `$checkbox-height` | `20px` | Размер чекбокса по высоте |
| `$checkbox-icon-background` | `transparent` | Фон иконки |
| `$checkbox-icon-background--checked` | `#000` | Фон иконки при наведении курсора |
| `$checkbox-icon-width` | `18px` | Размер иконки по ширине |
| `$checkbox-icon-height` | `18px` | Размер иконки по высоте |
| `$checkbox-icon-top` | `unquote("calc(50% - #{$checkbox-icon-height} / 2)")` | Абсолютное позиционирование иконки по верхнему краю |
| `$checkbox-icon-left` | `unquote("calc(50% - #{$checkbox-icon-width} / 2)")` | Абсолютное позиционирование иконки по левому краю |
| `$checkbox-label-color` | `#000` | Цвет текста |
