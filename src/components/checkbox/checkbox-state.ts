'use strict';

export namespace CheckboxState {
	export interface IState {
		hovered: boolean;
		focused: boolean;
	}
}
