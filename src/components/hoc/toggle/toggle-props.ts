'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme } from '@timcowebapps/react.utils';

export namespace ToggleProps {
	export interface IExternalProps {
		// Empty
	}

	export interface IInjectedProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof ToggleProps.IInjectedProps
		 */
		scheme: IJsonScheme;
	}
}
