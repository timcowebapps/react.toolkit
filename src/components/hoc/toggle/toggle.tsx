'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, JsonSchemeHelpers, IJsonSchemeMap, Guid
} from '@timcowebapps/react.utils';
import { ToggleProps } from './toggle-props';
import { ToggleState, ToggleStateFlags } from './toggle-state';

export function withToggleableHoC(Child: React.ComponentType<ToggleProps.IInjectedProps>) {
	return class extends React.Component<ToggleProps.IInjectedProps & ToggleProps.IExternalProps, ToggleState.IState> {

		//#region Статические переменные

		public static displayName = `Toggle(${Child.displayName || Child.name})`;

		//#endregion

		//#region Приватные методы

		debounceStateChange = _.debounce((value: ToggleStateFlags) => {
			this.setState({ currentState: value });
		}, _.merge({}, Child.defaultProps, this.props).scheme.properties.debounceWait);

		/**
		 * Меняет флаги состояния при запросе на открытие компонента.
		 * 
		 * @class Toggle
		 * @public
		 */
		onRequestOpen = () => {
			let currState = this.state.currentState;
			currState &= ~ToggleStateFlags.CLOSED;
			currState = ToggleStateFlags.OPEN | ToggleStateFlags.OPENING;

			this.setState({ currentState: currState }, () => {
				currState &= ~ToggleStateFlags.OPENING;
				this.debounceStateChange(currState);
			});
		}

		/**
		 * Меняет флаги состояния при запросе на закрытие компонента.
		 * 
		 * @class Toggle
		 * @public
		 */
		onRequestClose = () => {
			let currState = this.state.currentState
			currState &= ~ToggleStateFlags.OPEN;
			currState = ToggleStateFlags.CLOSED | ToggleStateFlags.CLOSING;

			this.setState({ currentState: currState }, () => {
				currState &= ~ToggleStateFlags.CLOSING;
				this.debounceStateChange(currState);
			});
		}

		//#endregion

		/**
		 * Конструктор класса.
		 * 
		 * @class Toggle
		 * @public
		 * @constructor
		 * @param {ResultProps} props Свойства компонента.
		 */
		public constructor(props?: ToggleProps.IInjectedProps & ToggleProps.IExternalProps) {
			super(props);

			this.state = {
				currentState: this.props.scheme.properties.hidden
					? ToggleStateFlags.CLOSED
					: ToggleStateFlags.OPEN
			};
		}

		/**
		 * Компонент получает новые props.
		 * Этод метод не вызывается в момент первого render'a.
		 * 
		 * @class Toggle
		 * @public
		 * @param {ResultProps} nextProps Новые свойства.
		 */
		public componentWillReceiveProps(nextProps: ToggleProps.IInjectedProps & ToggleProps.IExternalProps): void {
			if (this.props.scheme.properties.hidden !== nextProps.scheme.properties.hidden) {
				if (nextProps.scheme.properties.hidden) this.onRequestClose();
				else this.onRequestOpen();
			}
		}

		/**
		 * Отрисовывает компонент.
		 * 
		 * @class Toggle
		 * @public
		 */
		public render() {
			const { properties, items } = this.props.scheme;
			const { stylesheet, block, modifiers } = properties.classes;

			return <Child {..._.merge({}, {
				scheme: {
					properties: {
						state: this.state.currentState
					}
				}
			}, this.props)} />;
		}
	}
}
