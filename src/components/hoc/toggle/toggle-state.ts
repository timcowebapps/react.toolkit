'use strict';

import {
	EnumHelpers
} from '@timcowebapps/react.utils';

export enum ToggleStateFlags {
	NONE,
	OPENING = 1 << 0,
	OPEN = 1 << 1,
	CLOSING = 1 << 2,
	CLOSED = 1 << 3
}

export namespace ToggleHelpers {
	export function hasBeforeOpening(toggleState: any): boolean {
		return EnumHelpers.hasFlags(toggleState, ToggleStateFlags.OPENING | ToggleStateFlags.OPEN)
	}

	export function hasAfterOpening(toggleState: any): boolean {
		return !EnumHelpers.hasFlags(toggleState, ToggleStateFlags.OPENING)
			&& EnumHelpers.hasFlags(toggleState, ToggleStateFlags.OPEN);
	}

	export function hasBeforeClosing(toggleState: any): boolean {
		return EnumHelpers.hasFlags(toggleState, ToggleStateFlags.CLOSING | ToggleStateFlags.CLOSED);
	}

	export function hasAfterClosing(toggleState: any): boolean {
		return !EnumHelpers.hasFlags(toggleState, ToggleStateFlags.CLOSING)
			&& EnumHelpers.hasFlags(toggleState, ToggleStateFlags.CLOSED);
	}
}

export namespace ToggleState {
	export interface IState {
		currentState: ToggleStateFlags;
	}
}
