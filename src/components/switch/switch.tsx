'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, JsonSchemeHelpers, HtmlTagTypes
} from '@timcowebapps/react.utils';
import { SwitchProps } from './switch-props';
import { SwitchState } from './switch-state';
import Button from '../button/button';

/**
 * Микрокомпонент переключателя.
 */
export default class Switch extends React.Component<SwitchProps.IProps, SwitchState.IState> {
	//#region Статические переменные

	public static displayName: string = "Switch";
	public static propTypes: PropTypes.ValidationMap<SwitchProps.IProps> = SwitchProps.types;
	public static defaultProps: SwitchProps.IProps = SwitchProps.defaults;

	//#endregion

	private _handleClickDebounced;

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Switch
	 * @private
	 */
	private _getInitialState(): SwitchState.IState {
		return {
			checked: this.props.scheme.defaults.checked
		};
	}

	/**
	 * Коллбек изменения состояния переключателя.
	 * 
	 * @class Switch
	 * @public
	 * @param event 
	 */
	private _handleClick(event: React.SyntheticEvent<HTMLInputElement>): void {
		event.persist();

		this.setState({ checked: !this.state.checked });

		if (this.props.scheme.properties.onChange)
			this.props.scheme.properties.onChange(event, this.state.checked);
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Switch
	 * @public
	 * @constructor
	 * @param {SwitchProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: SwitchProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class Switch
	 * @public
	 */
	public componentWillMount(): void {
		this.props = _.merge({}, Switch.defaultProps, this.props);

		this._handleClickDebounced = _.debounce(this._handleClick, this.props.scheme.properties.debounceWait, {
			leading: true,
			maxWait: this.props.scheme.properties.debounceWait,
			trailing: false
		});
	}

	/**
	 * Вызывается сразу перед тем, как компонент будет удален из DOM.
	 * 
	 * @class Switch
	 * @public
	 */
	public componentWillUnmount(): void {
		this._handleClickDebounced.cancel();
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Switch
	 * @public
	 */
	public render() {
		this.props = _.merge({}, Switch.defaultProps, this.props);

		const { properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers, extract } = properties.classes;

		return (
			<Button scheme={{
				uniqueId: this.props.scheme.uniqueId,
				properties: {
					htmlTag: HtmlTagTypes.Button,
					onClick: (event: React.SyntheticEvent<HTMLInputElement>) => this._handleClickDebounced(event),
					classes: {
						stylesheet: stylesheet,
						block: block,
						modifiers: _.union(modifiers, [this.state.checked ? "checked" : null]),
						extract: extract
					}
				},
				items: items
			}} />
		)
	}
}
