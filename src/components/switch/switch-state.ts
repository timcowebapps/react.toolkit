'use strict';

export namespace SwitchState {
	export interface IState {
		/**
		 * Выбранное значение переключателя.
		 * 
		 * @type {boolean}
		 * @memberof SwitchState.IState
		 */
		checked: boolean;
	}
}
