'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme, Guid } from '@timcowebapps/react.utils';

export namespace SwitchProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof SwitchProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			type: PropTypes.string,
			defaults: PropTypes.shape({
				checked: PropTypes.boolean
			}),
			properties: PropTypes.shape({
				debounceWait: PropTypes.number,
				onChange: PropTypes.func,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object,
					block: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string),
					extract: PropTypes.string
				})
			})
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			type: "SwitchButton",
			defaults: {
				checked: false
			},
			properties: {
				debounceWait: 2500,
				onChange: undefined,
				classes: {
					stylesheet: null,
					block: "switch",
					modifiers: []
				}
			}
		}
	}
}
