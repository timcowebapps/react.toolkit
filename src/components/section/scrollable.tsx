'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme, BEM } from '@timcowebapps/react.utils';
import { EventMng } from './event-mng';
import { ScrollMng, HScrollDirection } from './scroll-mng';
import { ScrollableProps } from './scrollable-props';
import { ScrollableState } from './scrollable-state';
import ScrollableItem from './scrollableItem';

export default class Scrollable extends React.Component<ScrollableProps.IProps, ScrollableState.IState> {
	//#region Статические переменные

	public static displayName: string = "Scrollable";
	public static propTypes: PropTypes.ValidationMap<ScrollableProps.IProps> = ScrollableProps.types;
	public static defaultProps: ScrollableProps.IProps = ScrollableProps.defaults;

	//#endregion

	//#region Приватные переменные

	/** HTML элемент контейнера. */
	private _container: HTMLElement;

	/** HTML элементы секций. */
	private _sects: Array<ScrollableItem>;

	/** Готов к изменению текущей секцию. */
	private _isReadyToChange: boolean;

	/** Менеджер событий. */
	private _eventManager: EventMng;

	/** Менеджер прокрутки. */
	private _scrollManager: ScrollMng;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Scrollable
	 * @private
	 */
	private _getInitialState(): ScrollableState.IState {
		return {
			currentSectionIdx: -1
		}
	}

	//#endregion

	//#region Геттеры менеджеров

	/** Получает менеджер событий. */
	public getEventManager(): EventMng { return this._eventManager }

	/** Получает менеджер прокрутки. */
	public getScrollManager(): ScrollMng { return this._scrollManager }

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Scrollable
	 * @public
	 * @constructor
	 * @param {ScrollableProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ScrollableProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._eventManager = new EventMng(this);
		this._scrollManager = new ScrollMng(this);

		this._container = null;
		this._sects = [];
		this._isReadyToChange = true;
	}

	/**
	 * Компонент получает новые props.
	 * Этод метод не вызывается в момент первого render'a.
	 * 
	 * @class Scrollable
	 * @public
	 * @param {ScrollableProps.IProps} nextProps Новые свойства.
	 */
	public componentWillReceiveProps(nextProps: ScrollableProps.IProps): void {
		if (this.state.currentSectionIdx !== nextProps.scheme.properties.current) {
			setTimeout(() => {
				this.changeCurrentSection(this.props.scheme.properties.current);
			}, 1000);
		}
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public componentWillMount(): void {
		this.props = _.merge({}, Scrollable.defaultProps, this.props);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public componentDidMount(): void {
		this._eventManager.register();
		this._container.style.height = window.innerHeight + "px";

		setTimeout(() => {
			var hash = window.location.hash.replace("#", "");
			if (this.props.scheme.properties.useHash && _.isEmpty(hash)) {
				// Empty
			}

			this.changeCurrentSection(this.props.scheme.defaults.currentItemIdx);
		}, 1000);
	}

	/**
	 * Вызывается прямо перед render, когда новые props и state получены.
	 * В этом методе нельзя вызывать setState.
	 * 
	 * @class Scrollable
	 * @public
	 * @param {ScrollableProps.IProps} nextProps Новые значения свойств.
	 * @param {ScrollableState.IState} nextState Новые значения состояния.
	 */
	public componentWillUpdate(nextProps: ScrollableProps.IProps, nextState: ScrollableState.IState): void {
		// Empty
	}

	/**
	 * Вызывается сразу после render.
	 * Не вызывается в момент первого render'а компонента.
	 * 
	 * @class Scrollable
	 * @public
	 * @param {ScrollableProps.IProps} prevProps Предыдушее значение свойств.
	 * @param {ScrollableState.IState} prevState Предыдущее значение состояния.
	 */
	public componentDidUpdate(prevProps: ScrollableProps.IProps, prevState: ScrollableState.IState): void {
		this.props = _.merge({}, Scrollable.defaultProps, this.props);

		if (this.state.currentSectionIdx !== prevState.currentSectionIdx) {
			if (this.props.scheme.properties.useHash) {
				this.state.currentSectionIdx >= 0
					? window.location.hash = "#" + (this.getSectionByIdx(this.state.currentSectionIdx) as Element).id
					: window.location.hash = "";
			}

			new Promise(resolve => {
				let timeout = setTimeout(() => {
					clearTimeout(timeout);
					resolve(true);
				}, this.props.scheme.properties.transitionDelay)
			}).then(() => { this._isReadyToChange = true });
		}
	}

	/**
	 * Вызывается сразу перед тем, как компонент будет удален из DOM.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public componentWillUnmount(): void {
		this._eventManager.unregister();
	}

	/**
	 * Получает HTML элемент контейнера.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public getHtmlContainer(): HTMLElement {
		return this._container;
	}

	/**
	 * Получает секцию по индексу.
	 * 
	 * @class Scrollable
	 * @public
	 * @param {number} idx Индекс секции.
	 */
	public getSectionByIdx(idx: number): Element | Text {
		return ReactDOM.findDOMNode(this._sects[idx]);
	}

	/**
	 * Получает количество секций.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public getSectionCount(): number {
		return this._sects.length;
	}

	/**
	 * Изменяет текущий раздел.
	 * 
	 * @class Scrollable
	 * @public
	 * @param {number} sectionIdx Индекс секции, на который осуществляется переход.
	 */
	public changeCurrentSection(sectionIdx: number) {
		if (this._isReadyToChange) {
			this._isReadyToChange = false;

			this.setState({ currentSectionIdx: sectionIdx });

			if (this.props.scheme.properties.onChange)
				this.props.scheme.properties.onChange(sectionIdx);
		}
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Scrollable
	 * @public
	 */
	public render(): JSX.Element {
		const { stylesheet, block } = this.props.scheme.properties.classes;

		const items = (this.props.scheme.items as Array<IJsonScheme>).map((value: IJsonScheme, key: number) =>
			<ScrollableItem key={key} ref={node => this._sects[key] = node} scheme={
				_.merge({}, value, {
					properties: {
						classes: {
							stylesheet: stylesheet,
							block: block,
							modifiers: [this.state.currentSectionIdx == key ? "current" : ""]
						},
						children: null
					}
				})
			}>{value.properties.children}</ScrollableItem>
		);

		return (
			<div ref={node => this._container = node} className={stylesheet[block]}>
				{items}
			</div>
		);
	}
}
