'use strict';

import { PlatformHelpers } from '@timcowebapps/react.utils';
import Scrollable from './scrollable';

export class EventMng {
	//#region Приватные переменные

	private _target: Scrollable;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class EventMng
	 * @public
	 * @constructor
	 * @param {Scrollable} target Компонент контейнера.
	 */
	public constructor(target: Scrollable) {
		this._target = target;
	}

	/**
	 * Регистрирует обработчики событий.
	 * 
	 * @class EventMng
	 * @public
	 */
	public register(): void {
		this._target.getHtmlContainer().addEventListener("mousewheel", (event: MouseWheelEvent) => this.onMouseWheel(event));
		this._target.getHtmlContainer().addEventListener("DOMMouseScroll", (event: MouseWheelEvent) => this.onMouseWheel(event));
		
		window.addEventListener("resize", (event: Event) => this.onResize(event));
	}

	/**
	 * Удаляет обработчики события, которые были зарегистрированы.
	 * 
	 * @class EventMng
	 * @public
	 */
	public unregister(): void {
		this._target.getHtmlContainer().removeEventListener("mousewheel", this.onMouseWheel);
		this._target.getHtmlContainer().removeEventListener("DOMMouseScroll", this.onMouseWheel);
		
		window.removeEventListener('resize', this.onResize);
	}

	//#region События

	/**
	 * Обрабатывает событие, возникшее при прокрутки мыши.
	 * 
	 * @class EventMng
	 * @public
	 * @param {MouseWheelEvent} event Свойства события.
	 */
	protected onMouseWheel(event: MouseWheelEvent): void {
		return this._target.getScrollManager().handleMouseWheel(event);
	}

	/**
	 * Обрабатывает событие, возникшее при изменении размера окна браузера.
	 * 
	 * @class EventMng
	 * @public
	 * @param {Event} event Свойства события.
	 */
	protected onResize(event: Event): void {
		this._target.getHtmlContainer().style.height = PlatformHelpers.getClientSize().y + "px";
	}

	//#endregion
}
