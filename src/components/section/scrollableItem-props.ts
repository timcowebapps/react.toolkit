'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace ScrollableItemProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof ScrollableItemProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				classes: PropTypes.shape({
					stylesheet: PropTypes.object,
					block: PropTypes.string,
					element: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string),
					extra: PropTypes.string
				}),
				children: PropTypes.oneOfType([
					PropTypes.arrayOf(PropTypes.node),
					PropTypes.node
				])
			})
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				classes: {
					stylesheet: null,
					block: "scrollable",
					element: "item",
					modifiers: [""],
					extra: ""
				},
				children: []
			}
		}
	}
}
