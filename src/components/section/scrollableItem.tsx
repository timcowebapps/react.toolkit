'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import { Classes, BEM } from '@timcowebapps/react.utils';
import { ScrollableItemProps } from './scrollableItem-props';

export default class ScrollableItem extends React.Component<any, any> {
	//#region Статические переменные

	public static displayName: string = "ScrollableItem";
	public static propTypes: PropTypes.ValidationMap<ScrollableItemProps.IProps> = ScrollableItemProps.types;
	public static defaultProps: ScrollableItemProps.IProps = ScrollableItemProps.defaults;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ScrollableItem
	 * @public
	 * @constructor
	 * @param {any} props Свойства компонента.
	 */
	public constructor(props?: any) {
		super(props);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class ScrollableItem
	 * @public
	 */
	public render(): JSX.Element {
		const scheme = _.merge({}, ScrollableItem.defaultProps.scheme, this.props.scheme);
		const { stylesheet, block, element, modifiers, extra } = scheme.properties.classes;

		return React.createElement("div", {
			id: scheme.uniqueId,
			className: Classes.many(BEM.block(stylesheet, block).element(element).modifiers(modifiers), stylesheet[extra])
		}, this.props.children);
	}
}
