'use strict';

import Scrollable from './scrollable';

/** Горизонтальное направление прокрутки. */
export enum HScrollDirection {
	Up = 1,
	Down = -1
}

export class ScrollMng {
	//#region Приватные переменные

	private _target: Scrollable;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class ScrollMng
	 * @public
	 * @constructor
	 * @param {Scrollable} target Компонент контейнера.
	 */
	public constructor(target: Scrollable) {
		this._target = target;
	}

	/**
	 * Реакция на событие прокрутки мышки.
	 * 
	 * @class ScrollMng
	 * @public
	 * @param {MouseWheelEvent} event Свойства события.
	 */
	public handleMouseWheel(event: MouseWheelEvent): void {
		const direction = Math.max(-1, Math.min(1, (event.wheelDelta || -event.detail)));
		if (direction === HScrollDirection.Down && this._target.state.currentSectionIdx !== this._target.getSectionCount() - 1) {
			this._target.changeCurrentSection(this._target.state.currentSectionIdx + 1);
		} else if (direction === HScrollDirection.Up && this._target.state.currentSectionIdx !== 0) {
			this._target.changeCurrentSection(this._target.state.currentSectionIdx - 1);
		}
	}
}
