'use strict';

export namespace ScrollableState {
	export interface IState {
		/**
		 * Индекс текущей секции.
		 *
		 * @type {number}
		 * @memberof ScrollableState.IState
		 */
		currentSectionIdx: number;
	}
}
