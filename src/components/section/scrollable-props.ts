'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace ScrollableProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof ScrollableProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			defaults: PropTypes.shape({
				currentItemIdx: PropTypes.number,
			}),
			properties: PropTypes.shape({
				useHash: PropTypes.boolean,
				current: PropTypes.number,
				onChange: PropTypes.func,
				transitionDelay: PropTypes.number,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string
				})
			}),
			items: PropTypes.arrayOf(PropTypes.shape({
				uniqueId: PropTypes.string.isRequired,
				properties: PropTypes.shape({
					children: PropTypes.oneOfType([
						PropTypes.arrayOf(PropTypes.node),
						PropTypes.node
					])
				})
			}))
		}).isRequired
	};

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			defaults: {
				currentItemIdx: 0,
			},
			properties: {
				useHash: true,
				current: 0,
				onChange: undefined,
				transitionDelay: 2000,
				classes: {
					stylesheet: null,
					block: "scrollable"
				}
			},
			items: []
		}
	};
}
