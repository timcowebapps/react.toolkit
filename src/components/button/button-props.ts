'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme, Guid, HtmlTagTypes } from '@timcowebapps/react.utils';

export namespace ButtonProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof ButtonProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				htmlTag: PropTypes.number,
				onClick: PropTypes.func,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object,
					block: PropTypes.string
				})
			}),
			items: PropTypes.arrayOf(
				PropTypes.shape({
					properties: PropTypes.shape({
						
					})
				})
			)
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				htmlTag: HtmlTagTypes.Button,
				onClick: undefined,
				classes: {
					stylesheet: null,
					block: "btn"
				}
			},
			items: []
		}
	}
}
