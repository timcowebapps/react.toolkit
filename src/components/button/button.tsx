'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, IJsonSchemeMap, JsonSchemeHelpers,
	EnumTransform, EnumHelpers,
	ComponentTypes, HtmlTagTypes
} from '@timcowebapps/react.utils';
import Label from '../label/label';
import Icon from '../icon/icon';
import { ButtonProps } from './button-props';

/**
 * Микрокомпонент кнопки.
 */
export default class Button extends React.Component<ButtonProps.IProps, any> {
	//#region Статические переменные

	public static displayName: string = "Button";
	public static propTypes: PropTypes.ValidationMap<ButtonProps.IProps> = ButtonProps.types;
	public static defaultProps: ButtonProps.IProps = ButtonProps.defaults;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Button
	 * @public
	 * @constructor
	 * @param {ButtonProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: ButtonProps.IProps) {
		super(props);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Button
	 * @public
	 */
	public render() {
		this.props = _.merge({}, Button.defaultProps, this.props);

		const { properties } = this.props.scheme;
		const { stylesheet, block, modifiers, extract } = properties.classes;

		const items = this.props.scheme.items as Array<IJsonScheme>;

		let htmlAttrs = (props: any) => {
			let attributes: any = {}

			if (_.isEqual(props.htmlTag, HtmlTagTypes.A)) {
				attributes.href = props.to || "/";
			} else if (_.isEqual(props.htmlTag, HtmlTagTypes.Button)) {
				attributes.type = props.type || "button";
			}

			attributes.style = props.style || null;

			return attributes;
		};

		return React.createElement(EnumTransform.toStr(HtmlTagTypes, properties.htmlTag).toLowerCase(), {
			...htmlAttrs(properties),
			onClick: this.props.scheme.properties.onClick,
			className: Classes.many(
				BEM.block(stylesheet, block).element().modifiers(modifiers),
				stylesheet[extract]
			)
		}, items.map((value: IJsonScheme, key: number) => {
			switch (value.type) {
				case ComponentTypes.Node:
					return value.properties.content;
				case ComponentTypes.Icon:
					return <Icon key={this.props.scheme.uniqueId + "--icon-" + key} scheme={value} />
				default:
					return <span>The item is not defined</span>
			}
		}));
	}
}
