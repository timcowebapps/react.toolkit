##Компонент кнопки.

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.uniqueId` | `string` | `Guid.newGuid()` | **Уникальный** идентификатор компонента |
| `scheme.properties.onClick` | `function` | `undefined` | Обработчик клика по кнопке |
