'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	EnumTransform,
	BEM,
	IJsonScheme, IJsonSchemeMap, JsonSchemeHelpers,
	Guid
} from '@timcowebapps/react.utils';
import { TabSetProps } from './tabset-props';
import { TabSetState } from './tabset-state';
import Menu from '../menu/menu';
//import Panel from './panel';
import Panel from '../panel/panel';

export default class TabSet extends React.Component<TabSetProps.IProps, TabSetState.IState> {
	//#region Статические переменные

	public static displayName: string = "TabSet";
	public static propTypes: PropTypes.ValidationMap<TabSetProps.IProps> = TabSetProps.types;
	public static defaultProps = TabSetProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	//#region Приватные переменные

	/** HTML элементы вкладок. */
	private _panels: Array<Panel>;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class TabSet
	 * @private
	 */
	private _getInitialState(): TabSetState.IState {
		const { defaults, items } = this.props.scheme;

		return {
			current: {
				idx: defaults.currentItemIdx,
				uid: ((items as IJsonSchemeMap).length > 0) ? items[defaults.currentItemIdx].uniqueId : Guid.EMPTY
			}
		};
	}

	private _handleClick(data: { idx: number, uid: string }): void {
		if (this.state.current.idx !== data.idx) {
			const prev = this.state.current;
			const curr = data;

			this.setState({ current: curr });

			if (this.props.scheme.properties.onChange)
				this.props.scheme.properties.onChange({
					prev: prev,
					curr: curr,
				});
		}
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class TabSet
	 * @public
	 * @constructor
	 * @param {TabSetProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: TabSetProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._panels = [];

		this._handleClick = this._handleClick.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class TabSet
	 * @public
	 */
	public render(): JSX.Element {
		const items = this.props.scheme.items as Array<IJsonScheme>;
		const { stylesheet, block } = this.props.scheme.properties.classes;

		/** Добавляем роль элементам меню */
		items.map((element: IJsonScheme) => {
			return element.properties.role = "tab";
		});

		return (
			<div className={stylesheet[block]}>
				<Menu scheme={_.merge({}, this.props.scheme, {
					properties: {
						role: "tablist",
						selected: this.state.current.uid,
						onClick: this._handleClick
					}
				})} />
				{(this.props.scheme.properties.dynamically)
					? <Panel ref={node => this._panels[0] = node} scheme={
						_.merge({}, items[this.state.current.idx], {
							properties: {
								role: "tabpanel",
								classes: {
									stylesheet: stylesheet,
									block: block,
									element: "panel",
									modifiers: ["is-visible"]
								}
							}
						})
					} />
					: items.map((value: IJsonScheme, key: number) =>
						<Panel key={key} ref={node => this._panels[key] = node} scheme={
							_.merge({}, value, {
								properties: {
									role: "tabpanel",
									classes: {
										stylesheet: stylesheet,
										block: block,
										element: "panel",
										modifiers: [
											value.uniqueId === this.state.current.uid ? "is-visible" : "is-hidden"
										]
									}
								}
							})
						} />
					)}
			</div>
		);
	}
}
