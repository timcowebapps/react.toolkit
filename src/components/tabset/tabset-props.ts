'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace TabSetProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof TabSetProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				dynamically: PropTypes.boolean,
				classes: PropTypes.object,
				onChange: PropTypes.func
			}),
			items: PropTypes.arrayOf(
				PropTypes.shape({
					properties: PropTypes.shape({
						
					})
				})
			)
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: null,
			items: []
		}
	}
}
