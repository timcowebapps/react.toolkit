'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';
import { MenuItemProps } from './menu-item-props';

export namespace MenuProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof MenuProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			properties: PropTypes.shape({
				// Empty
			}),
			//items: PropTypes.arrayOf(MenuListItemProps.types.scheme)
		})
	}

	export const defaults: IProps = {
		scheme: {
			// Empty
		}
	}
}
