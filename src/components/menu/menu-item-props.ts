'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace MenuItemProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof MenuItemProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				role: PropTypes.string,
				selected: PropTypes.boolean,
				disabled: PropTypes.boolean,
				onClick: PropTypes.func,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string)
				}),
				content: PropTypes.oneOfType([
					PropTypes.arrayOf(PropTypes.node),
					PropTypes.node
				])
			})
		})
	}

	export const defaults: IProps = {
		scheme: {
			// Empty
		}
	}
}
