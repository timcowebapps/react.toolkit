'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	EnumTransform,
	Classes, BEM,
	IJsonScheme, JsonSchemeHelpers
} from '@timcowebapps/react.utils';
import { MenuProps } from './menu-props';
import { MenuState } from './menu-state';
import MenuItem from './menu-item';

export default class Menu extends React.Component<MenuProps.IProps, MenuState.IState> {
	//#region Статические переменные

	public static displayName: string = "Menu";
	public static propTypes: PropTypes.ValidationMap<MenuProps.IProps> = MenuProps.types;
	public static defaultProps = MenuProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	//#region Приватные переменные

	/** HTML элементы вкладок. */
	private _items: Array<MenuItem>;

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Menu
	 * @private
	 */
	private _getInitialState(): MenuState.IState {
		return {
			// Empty
		};
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Menu
	 * @public
	 * @constructor
	 * @param {MenuProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: MenuProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._items = [];
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Menu
	 * @public
	 */
	public render(): JSX.Element {
		const { stylesheet, block, modifiers } = this.props.scheme.properties.classes;

		return React.createElement("ul", {
			role: this.props.scheme.properties.role || undefined,
			className: Classes.many(BEM.block(stylesheet, block).element("list").modifiers([
				// Empty
			]), stylesheet["list-unstyled"])
		}, (this.props.scheme.items as Array<IJsonScheme>).map((value: IJsonScheme, idx: number) =>
			<MenuItem key={idx} ref={node => this._items[idx] = node} scheme={
				_.merge({}, value, {
					properties: {
						idx: idx,
						selected: this.props.scheme.properties.selected === value.uniqueId ? true : false,
						onClick: this.props.scheme.properties.onClick,
						classes: {
							stylesheet: stylesheet,
							block: block,
							modifiers: modifiers
						}
					}
				})
			} />
		));
	}
}
