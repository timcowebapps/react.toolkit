'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import { EnumTransform, BEM, JsonSchemeHelpers } from '@timcowebapps/react.utils';
import { MenuItemProps } from './menu-item-props';
import { MenuItemState } from './menu-item-state';

export default class MenuItem extends React.Component<MenuItemProps.IProps, MenuItemState.IState> {
	//#region Статические переменные

	public static displayName: string = "MenuItem";
	public static propTypes: PropTypes.ValidationMap<MenuItemProps.IProps> = MenuItemProps.types;
	public static defaultProps = MenuItemProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class MenuItem
	 * @private
	 */
	private _getInitialState(): MenuItemState.IState {
		return {
			//Empty
		};
	}

	private _handleClick(event: React.MouseEvent<HTMLElement>): void {
		if (this.props.scheme.properties.onClick)
			this.props.scheme.properties.onClick({
				uid: this.props.scheme.uniqueId,
				idx: this.props.scheme.properties.idx,
			});
	}

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class MenuItem
	 * @public
	 * @constructor
	 * @param {MenuItemProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: MenuItemProps.IProps) {
		super(props);

		this.state = this._getInitialState();

		this._handleClick = this._handleClick.bind(this);
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class MenuItem
	 * @public
	 */
	public render(): JSX.Element {
		const { classes, content } = this.props.scheme.properties;

		return React.createElement("li", {
			role: this.props.scheme.properties.role || undefined,
			className: BEM.block(classes.stylesheet, [classes.block, "list"]).element("list-item").modifiers([
				this.props.scheme.properties.selected ? "selected" : "",
				this.props.scheme.properties.disabled ? "disabled" : "",
			]),
			onClick: this._handleClick
		}, content);
	}
}
