'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as PropTypes from 'prop-types';
import {
	BEM,
	HtmlTagTypes, HtmlFieldTypes, ComponentTypes,
	EnumTransform,
	IJsonScheme, JsonSchemeHelpers
} from '@timcowebapps/react.utils';
import { FieldProps } from './field-props';
import { FieldState } from './field-state';
import Label from '../label/label';

export default class Field extends React.Component<FieldProps.IProps, FieldState.IState> {
	//#region Статические переменные

	public static displayName: string = "Field";
	public static propTypes: PropTypes.ValidationMap<FieldProps.IProps> = FieldProps.types;
	public static defaultProps = FieldProps.defaults; /*!< Свойства компонента по умолчанию. */

	//#endregion

	//#region Приватные методы

	/**
	 * Начальное состояние свойств по умолчанию.
	 * 
	 * @class Field
	 * @private
	 */
	private _getInitialState(): FieldState.IState {
		return {
			value: this.props.scheme.properties.value || "",
			empty: _.isEmpty(this.props.scheme.properties.value),
			valid: true,
			focused: false
		};
	}

	// #region Events

	private _handleChanged(event: React.ChangeEvent<HTMLInputElement>): void {
		if (this.props.scheme.properties.validate) {
			let target: any = event.target;
			if (this.props.scheme.properties.validate && this.props.scheme.properties.validate(target.value)) {
				this.setState({
					empty: _.isEmpty(event.target.value),
					valid: true
				});
			} else {
				this.setState({
					empty: _.isEmpty(event.target.value),
					valid: false
				});
			}
		}

		if (this.props.scheme.properties.onValueChange)
			this.props.scheme.properties.onValueChange(event);
	}

	/**
	 * Обработчик фокуса комнонента.
	 * 
	 * @class Field
	 * @private
	 * @param event 
	 */
	private _handleFocused(event: any/*JQueryEventObject*/): void {
		this.setState({ focused: true });
	}

	/**
	 * Обработчик снятия фокуса компонента.
	 * 
	 * @class Field
	 * @private
	 * @param event 
	 */
	private _handleBlurred(event: any/*JQueryEventObject*/): void {
		this.setState({ focused: false });
	}

	// #endregion

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Field
	 * @public
	 * @constructor
	 * @param {FieldProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: FieldProps.IProps) {
		super(props);

		this.state = this._getInitialState();
	}

	/**
	 * Компонент получает новые props.
	 * Этод метод не вызывается в момент первого render'a.
	 * 
	 * @class Field
	 * @public
	 * @param {FieldProps.IProps} nextProps Новые свойства.
	 */
	public componentWillReceiveProps(nextProps: FieldProps.IProps): void {
		if (nextProps.scheme.properties.value !== this.props.scheme.properties.value) {
			this.setState({ value: nextProps.scheme.properties.value });
		}
	}

	public isValid(): Boolean {
		if (this.props.scheme.properties.validate) {
			if (_.isEmpty(this.state.value) || !this.props.scheme.properties.validate(this.state.value)) {
				this.setState({ valid: false });
			} else {
				this.setState({ valid: true });
			}
		}

		return this.state.valid;
	}

	/**
	 * Отрисовывает форму ввода.
	 * 
	 * @class Field
	 * @private
	 */
	private _renderInput(uniqueId, name: string, classes: any, obj: any) {
		let attributes: any = {};

		const htmlTag = EnumTransform.toStr(HtmlTagTypes, obj.properties.htmlTag).toLowerCase() || "input";
		const htmlType = EnumTransform.toStr(HtmlFieldTypes, obj.properties.htmlType).toLowerCase() || "text";

		if (htmlTag === "textarea") {
			attributes.rows = obj.properties.rows;
			attributes.cols = obj.properties.cols;
			attributes.wrap = obj.properties.wrap;
		} else {
			attributes.type = htmlType;
		}

		if (attributes.type === "number") {
			attributes.min = obj.properties.min;// || Number.MIN_SAFE_INTEGER;
			attributes.max = obj.properties.max;// || Number.MAX_SAFE_INTEGER;
			attributes.step = obj.properties.step || 1;
		}

		attributes.name = name;
		attributes.id = uniqueId;
		attributes.value = this.props.scheme.properties.value;
		attributes.placeholder = obj.properties.placeholder || null;
		attributes.className = BEM.block(classes.stylesheet, classes.block).element("input").modifiers((obj.properties.classes) ? (obj.properties.classes.modifiers || []) : []);
		attributes.style = obj.properties.style || null;

		return React.createElement(htmlTag, {
			...attributes,
			onChange: this._handleChanged.bind(this),
			onFocus: this._handleFocused.bind(this),
			onBlur: this._handleBlurred.bind(this)
		});
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Field
	 * @public
	 */
	public render(): JSX.Element {
		const { uniqueId, properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers } = properties.classes;

		const labelScheme = JsonSchemeHelpers.getItemByType(items, ComponentTypes.Label);
		const inputScheme = JsonSchemeHelpers.getItemByType(items, ComponentTypes.Field);

		// for (var i = 0; i < properties.validators.length; ++i) {
		// 	console.log(properties.validators[i](this.state.value));
		// }

		var formGroupClasses = BEM.block(stylesheet, block).element().modifiers(_.union([
			this.state.valid ? "valid" : "error",
			this.state.focused ? "focused" : "unfocused",
		], modifiers));

		return (
			<div className={formGroupClasses} style={{ ...properties.style }}>
				{labelScheme ? <Label scheme={_.merge({}, {
					properties: {
						htmlTag: HtmlTagTypes.Label,
						associateWith: uniqueId,
						classes: {
							stylesheet: stylesheet,
							block: block,
							element: "label"
						}
					}
				}, labelScheme)} /> : null}
				{this._renderInput(uniqueId, properties.name, properties.classes, inputScheme)}
			</div>
		);
	}
}
