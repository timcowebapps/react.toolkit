##Компонент поля ввода.

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.uniqueId` | `string` | `Guid.newGuid()` | **Уникальный** идентификатор компонента |
| `scheme.properties.value` | `any` | `-` | Значение передаваемое в поле ввода |
| `scheme.properties.onValueChange` | `Function` | `-` | Событие изменения данных |
| `scheme.properties.rows` | `number` | `-` | Количество рядов. Свойство доступно, если тэг `textarea` |
| `scheme.properties.cols` | `number` | `-` | Количество колонок.Свойство доступно, если тэг `textarea` |
| `scheme.properties.wrap` | `string` | `-` | Свойство доступно, если тэг `textarea` |
| `scheme.properties.min` | `number` | `-` | Минимально допустимое значение. Свойство доступно, если тип `number` |
| `scheme.properties.max` | `number` | `-` | Максимально допустимое значение. Свойство доступно, если тип `number` |
| `scheme.properties.step` | `number` | `-` | Свойство доступно, если тип `number` |
