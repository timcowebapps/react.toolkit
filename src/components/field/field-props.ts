'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { Guid, IJsonScheme } from '@timcowebapps/react.utils';

export namespace FieldProps {
	export interface IProps extends React.Props<any> {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof FieldProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				//#region если тэг Textarea
				rows: PropTypes.number,
				cols: PropTypes.number,
				wrap: PropTypes.string,
				//#endregion
				//#region Если тип number
				min: PropTypes.number,
				max: PropTypes.number,
				step: PropTypes.number,
				//#endregion
				name: PropTypes.string.isRequired,
				value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
				validate: PropTypes.func,
				onValueChange: PropTypes.func,
				classes: PropTypes.object.isRequired
			}),
			items: PropTypes.arrayOf(
				PropTypes.shape({
					uniqueId: PropTypes.string,
					properties: PropTypes.oneOfType([
						PropTypes.shape({
							htmlTag: PropTypes.number.isRequired,
							htmlType: PropTypes.number.isRequired
						}),
						PropTypes.shape({
							text: PropTypes.string.isRequired
						})
					])
				})
			)
		}).isRequired
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: null,
			items: []
		}
	}
}
