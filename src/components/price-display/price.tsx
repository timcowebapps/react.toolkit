'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import {
	Classes, BEM,
	EnumTransform,
	HtmlTagTypes
} from '@timcowebapps/react.utils';
import { PriceProps } from './price-props';

const Price: React.StatelessComponent<PriceProps.IProps> = (props: PriceProps.IProps) => {
	const { properties } = _.merge({}, this.default.defaultProps.scheme, props.scheme);
	const { stylesheet, block, modifiers } = properties.classes;

	return React.createElement(EnumTransform.toStr(HtmlTagTypes, properties.htmlTag).toLowerCase(), {
		className: BEM.block(stylesheet, block).element().modifiers(modifiers)
	}, <React.Fragment><span className={BEM.block(stylesheet, block).element("amount").toStr()}>{properties.amount}</span>&nbsp;{properties.currency}</React.Fragment>);
};

Price.propTypes = PriceProps.types;
Price.defaultProps = PriceProps.defaults;

export default Price;
