'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	Guid,
	IJsonScheme,
	AlignTypes, TextAlign,
	HtmlTagTypes
} from '@timcowebapps/react.utils';

export namespace PriceProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonSchema}
		 * @memberof PriceProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				htmlTag: PropTypes.number,
				amount:PropTypes.number,
				currency: PropTypes.string,
				classes: PropTypes.shape({
					stylesheet: PropTypes.object.isRequired,
					block: PropTypes.string,
					modifiers: PropTypes.arrayOf(PropTypes.string)
				})
			})
		})
	};

	export const defaults = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				htmlTag: HtmlTagTypes.Span,
				amount: 0,
				currency: "",
				classes: {
					stylesheet: null,
					block: "price",
					modifiers: []
				}
			}
		}
	};
}
