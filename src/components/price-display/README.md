##Компонент отображения цены.

###Свойства scheme 

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.classes.stylesheet` | `IStylesheet` | `null` | Таблица стилей |
| `scheme.properties.classes.block` | `string` | `drawer` | Имя блока |
| `scheme.properties.classes.modifiers` | `Array<string>` | `-` | Имена модификаторов |
| `scheme.properties.htmlTag` | `number` | `HtmlTagTypes.Span` | Имя тега |
| `scheme.properties.amount` | `number` | `0` | Сумма |
| `scheme.properties.currency` | `string` | `-` | Валюта |

###Переменные sass

| Свойство | По умолчанию | Описание |
| :--- | :--- | :--- |
| `$price-name` | `price` | Имя |
| `$price-amount-fontColor` | `#333` | Цвет шрифта суммы |
| `$price-amount-fontSize` | `$body-fontSize` | Размер шрифта суммы |
| `$price-amount-fontWeight` | `normal` | Жирность шрифта суммы |
