##Компонент выдвижного ящика.

###Свойства scheme 

| Свойство | Тип | По умолчанию | Описание |
| :--- | :--- | :--- | :--- |
| `scheme.properties.classes.stylesheet` | `IStylesheet` | `null` | Таблица стилей |
| `scheme.properties.classes.block` | `string` | `drawer` | Имя блока |
| `scheme.properties.classes.modifiers` | `Array<string>` | `-` | Имена модификаторов |

###Переменные sass

| Свойство | По умолчанию | Описание |
| :--- | :--- | :--- |
| `$drawer-name` | `drawer` | Имя выдвижного ящика |
