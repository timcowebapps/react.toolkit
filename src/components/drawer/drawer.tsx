'use strict';

import * as _ from 'lodash';
import * as React from 'react';
import * as PropTypes from 'prop-types';
import {
	IStylesheet, Classes, BEM,
	IJsonScheme, JsonSchemeHelpers, IJsonSchemeMap,
	EnumTransform, EnumHelpers,
	ComponentTypes,
	PositionTypes
} from '@timcowebapps/react.utils';
import Backdrop from '../backdrop/backdrop';
import Panel from '../panel/panel';
import { withToggleableHoC } from '../hoc/toggle/toggle';
import { ToggleProps } from '../hoc/toggle/toggle-props';
import { ToggleState, ToggleStateFlags, ToggleHelpers } from '../hoc/toggle/toggle-state';
import { DrawerProps } from './drawer-props';
import { DrawerState } from './drawer-state';

class Drawer extends React.Component<DrawerProps.IProps, DrawerState.IState> {
	//#region Статические переменные

	public static displayName: string = "Drawer";
	public static propTypes: PropTypes.ValidationMap<DrawerProps.IProps> = DrawerProps.types;
	public static defaultProps: DrawerProps.IProps = DrawerProps.defaults;

	//#endregion

	//#region Приватные переменные

	private _drawerRef: HTMLElement;

	//#endregion

	/**
	 * Конструктор класса.
	 * 
	 * @class Drawer
	 * @public
	 * @constructor
	 * @param {DrawerProps.IProps} props Свойства компонента.
	 */
	public constructor(props?: DrawerProps.IProps) {
		super(props);
	}

	/**
	 * Компонент будет примонтирован.
	 * В данный момент у нас нет возможности посмотреть DOM элементы.
	 * 
	 * @class Drawer
	 * @public
	 */
	public componentWillMount(): void {
		this.props = _.merge({}, Drawer.defaultProps, this.props);
	}

	/**
	 * Компонент примонтировался.
	 * В данный момент у нас есть возможность использовать refs.
	 * 
	 * @class Drawer
	 * @public
	 */
	public componentDidMount(): void {
		this._drawerRef.setAttribute("data-position", EnumTransform.toStr(PositionTypes, this.props.scheme.properties.position).toLowerCase());
	}

	/**
	 * Отрисовывает компонент.
	 * 
	 * @class Drawer
	 * @public
	 */
	public render() {
		this.props = _.merge({}, Drawer.defaultProps, this.props);

		const { properties, items } = this.props.scheme;
		const { stylesheet, block, modifiers } = properties.classes;

		return React.createElement("div", {
			ref: node => { this._drawerRef = node },
			className: BEM.block(stylesheet, block).element().modifiers(_.union([
				ToggleHelpers.hasBeforeOpening(properties.state) ? "opening" : null,
				ToggleHelpers.hasAfterOpening(properties.state) ? "open" : null,
				ToggleHelpers.hasBeforeClosing(properties.state) ? "closing" : null,
				ToggleHelpers.hasAfterClosing(properties.state) ? "closed" : null
			], modifiers))
		}, (items as IJsonSchemeMap).map((item: IJsonScheme, idx: number) => {
			switch (item.type) {
				case ComponentTypes.Backdrop:
					return <Backdrop key={idx} scheme={{
						properties: {
							active: !ToggleHelpers.hasAfterClosing(properties.state),
							effect: item.properties.effect,
							onClick: item.properties.onClick,
							classes: {
								stylesheet: stylesheet,
								block: block,
								element: "backdrop"
							}
						}
					}} />
				case ComponentTypes.Panel:
					return <Panel key={idx} scheme={{
						properties: {
							content: item.properties.content,
							classes: {
								stylesheet: stylesheet,
								block: block,
								element: "panel",
								modifiers: [
									ToggleHelpers.hasAfterOpening(properties.state) && !ToggleHelpers.hasBeforeClosing(properties.state) ? "is-visible" : null
								]
							}
						}
					}} />
				default:
					return <span>The item is not defined</span>
			}
		}));
	}
}

export default withToggleableHoC(Drawer);
