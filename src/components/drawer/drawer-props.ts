'use strict';

import * as React from 'react';
import * as PropTypes from 'prop-types';
import { IJsonScheme, PositionTypes, Guid } from '@timcowebapps/react.utils';

export namespace DrawerProps {
	export interface IProps {
		/**
		 * Схема.
		 * 
		 * @type {IJsonScheme}
		 * @memberof DrawerProps.IProps
		 */
		scheme: IJsonScheme;
	}

	export const types: PropTypes.ValidationMap<IProps> = {
		scheme: PropTypes.shape({
			uniqueId: PropTypes.string,
			properties: PropTypes.shape({
				hidden: PropTypes.boolean,
				position: PropTypes.number,
				debounceWait: PropTypes.number
			}),
			items: PropTypes.arrayOf(
				PropTypes.shape({
					properties: PropTypes.shape({
						
					})
				})
			)
		})
	}

	export const defaults: IProps = {
		scheme: {
			uniqueId: Guid.newGuid(),
			properties: {
				hidden: true,
				position: PositionTypes.Left,
				debounceWait: 1200
			},
			items: []
		}
	}
}
